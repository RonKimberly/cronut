<?php



require_once('./classes.php');

$storage_file = "./storage.txt";

unset($file_handle);


$csvfile = fopen($storage_file,'r');

while(!feof($csvfile)) {

	    $allfields[] = fgetcsv($csvfile);
	       
}


/* as we saw at the end of class, the $allfields variable above is an array composed of arrays.  Specifically, each array within the bigger array is an array composed of each of the four elements in the csv file (storage.txt) in order.  
 *
 * As such, you should now create an array of OBJECTS, of the class type "subject" (which is the class we have invented for this exercise) So, again, it should be an array of objects, composed of ALL of the objects in our "database" (which, in this case, is simply a flat-file)
 */



$subject_array = array();

foreach($allfields as $fieldrrow) {
    $currsub = new subject();
    
    $currsub->setFirstname($fieldrrow[0]);
    $currsub->setLastname($fieldrrow[1]);
    $currsub->setAge($fieldrrow[2]);
    $currsub->setIncome($fieldrrow[3]);
    
    $subject_array[] = $currsub;
        
}

$usrnmbr = 1;

echo "<h1><center>Form Report</h1></center>";

foreach ($subject_array as $personlist) {

    echo '<br>';
    echo "<h2> Person #$usrnmbr </h1>";
    echo 'Name: ';
    echo $personlist->getFirstname() . " " . $personlist->getLastname();
    echo "<br>";
    echo "Age: " . $personlist->getAge();
    echo "<br>";
    echo "Code name: " . $personlist->agetocolor() . " " . $personlist->incometofood();
    
    $usrnmbr++;
}
